package tripstrapstrull;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class AlumineNupp extends JPanel {

	private JButton taasta;
	private Laud m2nguLaud;
	
	// Alumine nupp taastab m�ngulaua, et saaks uuesti m�ngida
	public AlumineNupp() {
		
		setLayout(new FlowLayout());
		taasta = new JButton("Taasta");
		taasta.setBackground(Color.LIGHT_GRAY);
		taasta.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				m2nguLaud.taasta();
			}
		});
		add(taasta);
	}
	
	// Meetod lisab uue m�ngulaua
	public void lisaObjekt(Laud l) {
		m2nguLaud = l;
	}
}