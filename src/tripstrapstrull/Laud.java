package tripstrapstrull;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Laud extends JPanel implements ActionListener {

	private JButton N1, N2, N3, N4, N5, N6, N7, N8, N9;
	private M2nguMassiiv m2nguMassiiv;
	private boolean m2ngija = false;
	private int m2ngijaNumber = 1;

	public Laud() {
		
		// Luuakse laua tulpade ja veergude suurus
		setLayout(new GridLayout(3, 3));
		
		// Luuakse nupud
		N1 = new JButton();
		N2 = new JButton();
		N3 = new JButton();
		N4 = new JButton();
		N5 = new JButton();
		N6 = new JButton();
		N7 = new JButton();
		N8 = new JButton();
		N9 = new JButton();

		m2nguAlgus();
		
		// Lisatakse nupud
		add(N1);
		add(N2);
		add(N3);
		add(N4);
		add(N5);
		add(N6);
		add(N7);
		add(N8);
		add(N9);
		
		N1.addActionListener(this);
		N2.addActionListener(this);
		N3.addActionListener(this);
		N4.addActionListener(this);
		N5.addActionListener(this);
		N6.addActionListener(this);
		N7.addActionListener(this);
		N8.addActionListener(this);
		N9.addActionListener(this);
	}
	
	public void m2nguAlgus() {
		
		// Seatakse, milline on m�ngulaud iga m�ngu alguses
		m2nguMassiiv = new M2nguMassiiv(this);
		algneTekst();
		eemaldaK6ik(true);
		m2ngija = false; 
		m2ngijaNumber = 1; 
	}
	
	public void eemaldaK6ik(boolean e) {
		
		// Kui false, siis eemaldab k�ik nupud, et nende peale ei saaks vajutada
		N1.setEnabled(e);
		N2.setEnabled(e);
		N3.setEnabled(e);
		N4.setEnabled(e);
		N5.setEnabled(e);
		N6.setEnabled(e);
		N7.setEnabled(e);
		N8.setEnabled(e);
		N9.setEnabled(e);
	}
	public void algneTekst() {
		
		// Seatakse nuppude algne tekst, mida neil ei ole
		N1.setText("");
		N2.setText("");
		N3.setText("");
		N4.setText("");
		N5.setText("");
		N6.setText("");
		N7.setText("");
		N8.setText("");
		N9.setText("");
		
		// M��ratakse nuppude v�rv
		N1.setBackground(Color.GREEN);
		N2.setBackground(Color.GREEN);
		N3.setBackground(Color.GREEN);
		N4.setBackground(Color.GREEN);
		N5.setBackground(Color.GREEN);
		N6.setBackground(Color.GREEN);
		N7.setBackground(Color.GREEN);
		N8.setBackground(Color.GREEN);
		N9.setBackground(Color.GREEN);
	}
	
	public void taasta() {
		
		// See taastab m�ngu
		m2nguAlgus(); 
	}

	public void actionPerformed(ActionEvent e) {
		
		JButton vajutatudNupp = (JButton) e.getSource();
		
		// Kui vajutatakse mingit nuppu, siis selle v��rus saadetakse M2nguMassiivi klassi
		if (vajutatudNupp == N1) {
			m2nguMassiiv.massiiviT66tlemine(0, 0, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija); // Vajutatud nupule pannakse tekst	
			m2ngijaNumber = vahetaK2iku(m2ngija); // Vahetatakse m�ngijat
			eemaldaNupp(N1); // Eemaldatakse vajutatud nupp, et selle peale ei saaks rohkem vajutada
			
		} else if (vajutatudNupp == N2) {
			m2nguMassiiv.massiiviT66tlemine(0, 1, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija);
			m2ngijaNumber = vahetaK2iku(m2ngija);
			eemaldaNupp(N2);
			
		} else if (vajutatudNupp == N3) {
			m2nguMassiiv.massiiviT66tlemine(0, 2, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija);
			m2ngijaNumber = vahetaK2iku(m2ngija);
			eemaldaNupp(N3);
			
		} else if (vajutatudNupp == N4) {
			m2nguMassiiv.massiiviT66tlemine(1, 0, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija);
			m2ngijaNumber = vahetaK2iku(m2ngija);
			eemaldaNupp(N4);
			
		} else if (vajutatudNupp == N5) {
			m2nguMassiiv.massiiviT66tlemine(1, 1, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija);
			m2ngijaNumber = vahetaK2iku(m2ngija);
			eemaldaNupp(N5);
			
		} else if (vajutatudNupp == N6) {
			m2nguMassiiv.massiiviT66tlemine(1, 2, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija);
			m2ngijaNumber = vahetaK2iku(m2ngija);
			eemaldaNupp(N6);
			
		} else if (vajutatudNupp == N7) {
			m2nguMassiiv.massiiviT66tlemine(2, 0, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija);
			m2ngijaNumber = vahetaK2iku(m2ngija);
			eemaldaNupp(N7);
			
		} else if (vajutatudNupp == N8) {
			m2nguMassiiv.massiiviT66tlemine(2, 1, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija);
			m2ngijaNumber = vahetaK2iku(m2ngija);
			eemaldaNupp(N8);
			
		} else if (vajutatudNupp == N9) {
			m2nguMassiiv.massiiviT66tlemine(2, 2, m2ngijaNumber);
			tekst(vajutatudNupp, m2ngija);
			m2ngijaNumber = vahetaK2iku(m2ngija);
			eemaldaNupp(N9);
		}
	}

	public int vahetaK2iku(boolean viimane) {
		
		// Kui viimase m�ngija v��rtus oli false siis vahetatakse m�ngijat
		if (viimane == true) {
			m2ngija = false;
			return 1;
		} else if (viimane == false) {
			m2ngija = true;
			return 2;
		} else {
			return 0;
		}
	}
	
	// See meetod eemaldab nupu, et ei saaks muuta nupu vajutusega m�ngijat
	public void eemaldaNupp(JButton nupp) {
		
		nupp.setEnabled(false); 
		nupp.setBackground(Color.WHITE);
	}

	// Meetod m��rab nupule t�he, kas X v�i O
	public void tekst(JButton nupp, boolean m2ngija) {
		
		if (m2ngija == true) {
			nupp.setText("O");
			nupp.setFont(new Font("arial", Font.PLAIN, 40));
		} else  {
			nupp.setText("X");
			nupp.setFont(new Font("arial", Font.PLAIN, 40));
		}
	}
}