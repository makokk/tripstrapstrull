package tripstrapstrull;

import java.awt.*;
import javax.swing.*;

public class M2ng extends JFrame {
	
	private Laud m2nguLaud; 
	private AlumineNupp alumineNupp; 

	public M2ng() {
		
		setLayout(new BorderLayout());
		m2nguLaud = new Laud();
		alumineNupp = new AlumineNupp();
		alumineNupp.lisaObjekt(m2nguLaud);
		
		// Lisatakse m�ngulaud kogu laua keskele
		add(m2nguLaud, BorderLayout.CENTER);
		m2nguLaud.setBackground(Color.WHITE);
		
		// Lisatakse alumineNupp m�ngulaua alumisse ossa
		add(alumineNupp, BorderLayout.SOUTH);
		alumineNupp.setBackground(Color.WHITE);
		
		// Laua parameetrid
		setVisible(true);
		setSize(550, 550);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}