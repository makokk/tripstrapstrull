package tripstrapstrull;

import javax.swing.*;

public class M2nguMassiiv {

	private Laud laud;
	private int m2nguMassiiv[][];

	public M2nguMassiiv(Laud l) {
		
		// Luuakse massiiv kolme rea ja kolme veeruga
		m2nguMassiiv = new int[3][3];
		laud = l;
		
		// K�iakse massiivi read ja veerud �le ning pannakse need massiivi
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				m2nguMassiiv[i][j] = 0;
			}
		}
	}

	public void massiiviT66tlemine(int i, int j, int m2ngijaNumber) {
		
		// Igal nupul on oma v��rtus ja vajutatud nupu v��rtus pannakse massiivi
		m2nguMassiiv[i][j] = m2ngijaNumber;
		v6iduKontrollimine(m2ngijaNumber);
		System.out.println(m2nguMassiiv[i][j]);
	}

	public void v6iduKontrollimine(int m2ngijaNumber) {
		
		// See meetod kontrollib, kes v�itis Trips Traps Trulli reeglite kohaselt
		if ((m2nguMassiiv[0][0] == m2ngijaNumber && m2nguMassiiv[0][1] == m2ngijaNumber && m2nguMassiiv[0][2] == m2ngijaNumber)
		|| (m2nguMassiiv[1][0] == m2ngijaNumber && m2nguMassiiv[1][1] == m2ngijaNumber && m2nguMassiiv[1][2] == m2ngijaNumber)
		|| (m2nguMassiiv[2][0] == m2ngijaNumber && m2nguMassiiv[2][1] == m2ngijaNumber && m2nguMassiiv[2][2] == m2ngijaNumber)
		|| (m2nguMassiiv[0][0] == m2ngijaNumber && m2nguMassiiv[1][0] == m2ngijaNumber && m2nguMassiiv[2][0] == m2ngijaNumber)
		|| (m2nguMassiiv[0][1] == m2ngijaNumber && m2nguMassiiv[1][1] == m2ngijaNumber && m2nguMassiiv[2][1] == m2ngijaNumber)
		|| (m2nguMassiiv[0][2] == m2ngijaNumber && m2nguMassiiv[1][2] == m2ngijaNumber && m2nguMassiiv[2][2] == m2ngijaNumber)
		|| (m2nguMassiiv[0][0] == m2ngijaNumber && m2nguMassiiv[1][1] == m2ngijaNumber && m2nguMassiiv[2][2] == m2ngijaNumber)
		|| (m2nguMassiiv[2][0] == m2ngijaNumber && m2nguMassiiv[1][1] == m2ngijaNumber && m2nguMassiiv[0][2] == m2ngijaNumber)) {
			
			// Vastava m�ngija v�idu korral kuvatakse s�num
			if (m2ngijaNumber == 1) {
				ImageIcon ikoon = new ImageIcon("images.png");
				JOptionPane.showMessageDialog(laud, "M�ngija 1 v�itis", "Teade", JOptionPane.PLAIN_MESSAGE, ikoon);
			} else if (m2ngijaNumber == 2) {
				ImageIcon ikoon = new ImageIcon("images.png");
				JOptionPane.showMessageDialog(laud, "M�ngija 2 v�itis", "Teade", JOptionPane.PLAIN_MESSAGE, ikoon);
			}
			
			// Laua pealt eemaldatakse nuppudele vajutamise v�imalus
			laud.eemaldaK6ik(false);
		}
	}
}